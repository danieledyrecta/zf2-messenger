<?php
/**
 * Created by PhpStorm.
 * User: serpe
 * Date: 07/02/17
 * Time: 10:47
 */

namespace MessagesService\Exception;

class MessagesServicePushException extends MessagesServiceException
{
    function __constructor($msg = null, $code = null) {
        $msg  = $msg === null ? MessagesServiceException::MESSAGE_PUSH : $msg;
        $code = $code === null ? MessagesServiceException::ERROR_PUSH_ID : $code;

        parent::__construct($msg, $code);
    }

    public function getErrorMessage() {
        return MessagesServiceException::MESSAGE_PUSH;
    }
}