<?php
/**
 * Created by PhpStorm.
 * User: serpe
 * Date: 07/02/17
 * Time: 10:42
 */

namespace MessagesService\Exception;

class MessagesServiceException extends \Exception
{
    // Mail exceptions codes and messages
    const ERROR_MAIL_VALIDATOR_ID = 2000;
    const MESSAGE_MAIL_VALIDATOR = "A mandatory param is missing.";
    const ERROR_MAIL_JSON_MISSING_ID = 2010;
    const MESSAGE_MAIL_JSON_MISSING = "Email 'jsonParams' option is missing.";
    const ERROR_MAIL_JSON_VALIDATOR_ID = 2020;
    const MESSAGE_MAIL_JSON_VALIDATOR = "Email 'jsonParams' are not valid. Must be a JSON with following fields: {\"username\": \"your_username\",\"password\": \"your_password\",\"authentication\": \"authentication_type\",\"host\": \"your_smtp_host\",\"port\": 25,\"ssl\": null}.";
    const ERROR_MAIL_FROM_MISSING_ID = 2030;
    const MESSAGE_MAIL_FROM_MISSING = "Email 'from' option is missing.";
    const ERROR_MAIL_FROM_VALIDATOR_ID = 2040;
    const MESSAGE_MAIL_FROM_VALIDATOR = "Email 'from' (%s) is not considered valid.";
    const ERROR_MAIL_TO_MISSING_ID = 2050;
    const MESSAGE_MAIL_TO_MISSING = "Email 'to' option is missing.";
    const ERROR_MAIL_TO_VALIDATOR_ID = 2060;
    const MESSAGE_MAIL_TO_VALIDATOR = "Email 'to' (%s) is not considered valid.";
    const ERROR_MAIL_SMTP_VALIDATOR_ID = 2070;
    const MESSAGE_MAIL_SMTP_VALIDATOR = "Invalid SMTP parameters.";
    const ERROR_MAIL_ID = 2070;
    const MESSAGE_MAIL = "An error occurred: %s.";
    const ERROR_MAIL_PULL_ID = 2080;
    const MESSAGE_MAIL_PULL = "An error occurred on pulling email: %s.";
    const ERROR_MAIL_JSON_TEMPLATE_VALIDATOR_ID = 2090;
    const MESSAGE_MAIL_JSON_TEMPLATE_VALIDATOR = "Email 'templateParams' are not valid. Must be a JSON.";

    // SMS exceptions codes and messages
    const ERROR_SMS_VALIDATOR_ID = 3000;
    const MESSAGE_SMS_VALIDATOR = "A mandatory param is missing.";
    const ERROR_SMS_JSON_MISSING_ID = 3010;
    const MESSAGE_SMS_JSON_MISSING = "SMS 'jsonParams' option is missing.";
    const ERROR_SMS_JSON_VALIDATOR_ID = 3020;
    const MESSAGE_SMS_JSON_VALIDATOR = "SMS 'jsonParams' are not valid. Must be a JSON with 'username' and 'password' fields. For example: {\"username\": \"your_username\", \"password\": \"your_password\"}";
    const ERROR_SMS_FROM_MISSING_ID = 3030;
    const MESSAGE_SMS_FROM_MISSING = "SMS 'from' option is missing.";
    const ERROR_SMS_TO_MISSING_ID = 3040;
    const MESSAGE_SMS_TO_MISSING = "SMS 'to' option is missing.";
    const ERROR_SMS_TO_VALIDATOR_ID = 3050;
    const MESSAGE_SMS_TO_VALIDATOR = "SMS 'to' phone number (%s) is not considered valid.";
    const ERROR_SMS_ID = 3060;
    const MESSAGE_SMS = "An error occurred: %s.";
    const ERROR_SMS_PULL_ID = 3070;
    const MESSAGE_SMS_PULL = "An error occurred on pulling SMS: %s.";

    // Push exceptions codes and messages
    const ERROR_PUSH_VALIDATOR_ID = 4000;
    const MESSAGE_PUSH_VALIDATOR = "A mandatory param is missing.";
    const ERROR_PUSH_DEVICE_MISSING_ID = 4010;
    const MESSAGE_PUSH_DEVICE_MISSING = "Push 'device' option is missing.";
    const ERROR_PUSH_DEVICE_VALIDATOR_ID = 4020;
    const MESSAGE_PUSH_DEVICE_VALIDATOR = "Push 'device' option must be only [%s] string.";
    const ERROR_PUSH_JSON_MISSING_ID = 4030;
    const MESSAGE_PUSH_JSON_MISSING = "Push 'jsonParams' option is missing.";
    const ERROR_PUSH_JSON_VALIDATOR_ID = 4040;
    const MESSAGE_PUSH_JSON_VALIDATOR = "Push 'jsonParams' are not valid. Must be a JSON string. For example: {\"badge\": 2, \"sound\": \"example.aiff\"}";
    const ERROR_PUSH_CERTIFICATE_MISSING_ID = 4050;
    const MESSAGE_PUSH_CERTIFICATE_MISSING = "Push 'jsonParams' must have 'certificate' key.";
    const ERROR_PUSH_APIKEY_MISSING_ID = 4060;
    const MESSAGE_PUSH_APIKEY_MISSING = "Push 'jsonParams' must have 'apiKey' key.";
    const ERROR_PUSH_TO_MISSING_ID = 4070;
    const MESSAGE_PUSH_TO_MISSING = "Push 'to' option is missing.";
    const ERROR_PUSH_MODE_VALIDATOR_ID = 4080;
    const MESSAGE_PUSH_MODE_VALIDATOR = "Push 'mode' option must be only [%s] string.";
    const ERROR_PUSH_ID = 4090;
    const MESSAGE_PUSH = "An error occurred: %s.";
    const ERROR_PUSH_PULL_ID = 4100;
    const MESSAGE_PUSH_PULL = "An error occurred on pulling Push message: %s.";
}