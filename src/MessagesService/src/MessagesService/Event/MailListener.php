<?php
namespace MessagesService\Event;

use AcMailer\Event\MailEvent;
use MessagesService\Model\MessagesModel;

class MailListener extends \AcMailer\Event\AbstractMailListener
{
    /** @var MessagesModel */
    private $logger = null;

    public function setLogger(MessagesModel $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Called before sending the email
     * @param MailEvent $e
     * @return mixed
     */
    public function onPreSend(MailEvent $e)
    {
//        $this->log("onPreSend");
    }

    /**
     * Called after sending the email
     * @param MailEvent $e
     * @return mixed
     */
    public function onPostSend(MailEvent $e)
    {
        //$this->log("onPostSend\n");
    }

    /**
     * Called if an error occurs while sending the email
     * @param MailEvent $e
     * @return mixed
     */
    public function onSendError(MailEvent $e)
    {
        $this->log("ERROR: " . $e->getResult()->getMessage());
    }

    private function log($message)
    {
        if ($this->logger != null) {
            $this->logger->log("AC-MAILER", $message);
        }
    }
}