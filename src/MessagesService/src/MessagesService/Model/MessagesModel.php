<?php

namespace MessagesService\Model;

use MessagesService\Exception\MessagesServiceException,
    MessagesService\Exception\MessagesServiceMailException,
    MessagesService\Exception\MessagesServiceSmsException;
use MessagesService\Exception\MessagesServicePushException;
use MessagesService\Service\Factory;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Mail\Exception\InvalidArgumentException;
use MessagesService\Service\Factory as MessagesServiceFactory,
    MessagesService\Helper\AbstractLogger,
    MessagesService\Helper\DbHelper,
    MessagesService\Model\MessageModel as Message;
use Exception;
use Sly\NotificationPusher\PushManager,
    Sly\NotificationPusher\Exception\AdapterException,
    Sly\NotificationPusher\Exception\PushException;

class MessagesModel extends AbstractLogger
{
    const STATUS_UNKNOWN = 1000;
    const STATUS_READY = 0;
    const STATUS_RETRY = 10;
    const STATUS_SENT = 100;
    const STATUS_FAIL = 110;
    const STATUS_ABORTED = 120;
    const STATUS_UNINSTALLED = 200;

    const HASH_ID_PLACEHOLDER = "@hashId@";

    private static $sqlPullMailMsg = "INSERT INTO `queue_mail` (
                                            `id`, `type`, `params`, `template_layout`, `template_params`, `from`, `from_alias`, `reply_to`, `reply_to_alias`, `to`, `subject`, `content`, `tracking_url`, `num_tries`,
                                            `send_date`, `created_at`, `updated_at`, `deleted_at`)
                                         VALUES (
                                            NULL, :type, :params, :template_layout, :template_params, :from, :from_alias, :reply_to, :reply_to_alias, :to, :subject, :content, :tracking_url, 0,
                                            :send_date, NOW(), NOW(), NULL);";

    private static $sqlPullMailMsgs = "INSERT INTO `queue_mail` (
                                            `hash_id`, `type`, `params`, `template_layout`, `template_params`, `from`, `from_alias`, `reply_to`, `reply_to_alias`, `to`, `subject`, `content`, `tracking_url`, `num_tries`,
                                            `send_date`, `created_at`, `updated_at`)
                                         VALUES %s;";

    private static $sqlPullSmsMsg = "INSERT INTO `queue_sms` (
                                            `id`, `type`, `params`, `from`, `to`, `content`, `num_tries`,
                                            `send_date`, `created_at`, `updated_at`, `deleted_at`)
                                         VALUES (
                                            NULL, :type, :params, :from, :to, :content, 0,
                                            :send_date, NOW(), NOW(), NULL);";

    private static $sqlPullSmsMsgs = "INSERT INTO `queue_sms` (
                                            `hash_id`, `type`, `params`, `from`, `to`, `content`, `num_tries`,
                                            `send_date`, `created_at`, `updated_at`)
                                         VALUES %s;";

    private static $sqlPullPushNotificationMsg = "INSERT INTO `queue_push` (
                                            `id`, `type`, `params`, `mode`, `title`, `to`, `content`, `tracking_url`, `collapse_id`, `num_tries`,
                                            `send_date`, `created_at`, `updated_at`, `deleted_at`)
                                         VALUES (
                                            NULL, :type, :params, :mode, :title, :to, :content, :tracking_url, :collapse_id, 0,
                                            :send_date, NOW(), NOW(), NULL);";

    private static $sqlPullPushNotificationMsgs = "INSERT INTO `queue_push` (
                                            `hash_id`, `type`, `params`, `mode`, `title`, `to`, `content`, `tracking_url`, `collapse_id`, `num_tries`,
                                            `send_date`, `created_at`, `updated_at`)
                                         VALUES %s;";

    private static $sqlPushMailMsg = "SELECT *
                                         FROM `queue_mail`
                                         WHERE `type` = :type AND `num_tries` < :max_tries AND `status` < :status AND `send_date` < NOW()
                                         ORDER BY `created_at` ASC LIMIT :limit;";

    private static $sqlUpdMailQueue = "UPDATE `queue_mail`
                                         SET `status`=:status, `num_tries`=`num_tries`+1, `log_message`=:log, `updated_at`=NOW()
                                         WHERE id=:id;";

    private static $sqlUpdHashIdMailQueue = "UPDATE `queue_mail`
                                             SET `hash_id`=:hash, `tracking_url`=REPLACE(`tracking_url`, :placeholder, :hashp)
                                             WHERE id=:id;";

    private static $sqlGetMailQueueByHashId = "SELECT *
                                               FROM `queue_mail`
                                               WHERE `hash_id`=:hash;";

    private static $sqlAbortSendMailQueue = "UPDATE `queue_mail`
                                             SET `status`=:status
                                             WHERE `hash_id`=:hash;";

    private static $sqlAbortSendMailsQueue = "UPDATE `queue_mail`
                                             SET `status`=:status
                                             WHERE `hash_id` IN (%s);";

    private static $sqlPushSmsMsg = "SELECT *
                                         FROM `queue_sms`
                                         WHERE `type` = :type AND `num_tries` < :max_tries AND `status` < :status AND `send_date` < NOW()
                                         ORDER BY `created_at` ASC LIMIT :limit;";

    private static $sqlUpdSmsQueue = "UPDATE `queue_sms`
                                         SET
                                          `status`=:status, `num_tries`=`num_tries`+1, `log_message`=:log,
                                          `callback_id`=:callback_id, `updated_at`=NOW()
                                         WHERE id=:id;";

    private static $sqlUpdHashIdSmsQueue = "UPDATE `queue_sms`
                                             SET `hash_id`=:hash
                                             WHERE id=:id;";

    private static $sqlGetSmsQueueByHashId = "SELECT *
                                               FROM `queue_sms`
                                               WHERE `hash_id`=:hash;";

    private static $sqlAbortSendSmsQueue = "UPDATE `queue_sms`
                                             SET `status`=:status
                                             WHERE `hash_id`=:hash;";

    private static $sqlAbortSendSmssQueue = "UPDATE `queue_sms`
                                             SET `status`=:status
                                             WHERE `hash_id` IN (%s);";

    private static $sqlPushNotificationPushMsg = "SELECT *
                                                    FROM `queue_push`
                                                    WHERE `num_tries` < :max_tries AND `status` < :status AND `send_date` < NOW()
                                                    ORDER BY `created_at` ASC LIMIT :limit;";

    private static $sqlUpdNotificationPushQueue = "UPDATE `queue_push`
                                                   SET `status`=:status, `num_tries`=`num_tries`+1, `log_message`=:log, `updated_at`=NOW()
                                                   WHERE id=:id;";

    private static $sqlUpdHashIdNotificationPushQueue = "UPDATE `queue_push`
                                                         SET `hash_id`=:hash, `tracking_url`=REPLACE(`tracking_url`, :placeholder, :hashp)
                                                         WHERE id=:id;";

    private static $sqlGetNotificationPushQueueByHashId = "SELECT *
                                                           FROM `queue_push`
                                                           WHERE `hash_id`=:hash;";

    private static $sqlGetNotificationPushValidToken = "SELECT *
                                                        FROM `queue_push`
                                                        WHERE `to`=:token
                                                        ORDER BY `updated_at` DESC
                                                        LIMIT 1;";

    private static $sqlAbortSendNotificationPushQueue = "UPDATE `queue_push`
                                                         SET `status`=:status
                                                         WHERE `hash_id`=:hash;";

    private static $sqlAbortSendNotificationsPushQueue = "UPDATE `queue_push`
                                                         SET `status`=:status
                                                         WHERE `hash_id` IN (%s);";

    private static $sqlUpdateMailMsg = "UPDATE `queue_mail`
                                        SET 
                                            `params` = :params, `template_layout` = :template_layout, `template_params` = :template_params, `from` = :from,
                                            `from_alias` = :from_alias, `reply_to` = :reply_to, `reply_to_alias` = :reply_to_alias, `to` = :to, `subject` = :subject,
                                            `content` = :content, `tracking_url` = :tracking_url, `send_date` = :send_date, `updated_at` = NOW()
                                        WHERE `hash_id`=:hash;";

    private static $sqlUpdateMailMsgByIds = "UPDATE `queue_mail`
                                             SET 
                                                `subject` = :subject, `content` = :content, `send_date` = :send_date, `updated_at` = NOW()
                                             WHERE `hash_id`= IN (%s)";

    private static $sqlUpdateSmsMsg = "UPDATE `queue_sms`
                                       SET 
                                          `params` = :params, `from` = :from, `to` = :to, `content` = :content, 
                                          `send_date` = :send_date,`updated_at` = NOW()
                                       WHERE `hash_id`=:hash;";

    private static $sqlUpdateSmsMsgByIds = "UPDATE `queue_sms`
                                            SET `content` = :content, `send_date` = :send_date,`updated_at` = NOW()
                                            WHERE `hash_id` IN (%s)";

    private static $sqlUpdatePushMsg = "UPDATE `queue_push`
                                        SET
                                            `params` = :params, `mode` = :mode, `title` = :title, `to` = :to, `content` = :content, 
                                            `tracking_url` = :tracking_url, `collapse_id` = :collapse_id, `send_date` = :send_date, `updated_at` = NOW()
                                        WHERE `hash_id`=:hash;";

    private static $sqlUpdatePushMsgByIds = "UPDATE `queue_push`
                                             SET
                                                `title` = :title, `content` = :content, `send_date` = :send_date, `updated_at` = NOW()
                                             WHERE `hash_id` IN (%s)";

    private $log_format = "#%s | %s (%s/%s) | %s";

    private $verboseMode = false;
    private $logMode = false;
    private $logType = null;
    private $messageType = null;
    private static $notificationHash = null;

    private $message = null;

    protected $dbHelper;
    protected $db;

    /**
     * MessagesModel constructor.
     * @param ServiceLocatorInterface $serviceLocator
     * @param $type
     * @throws Exception
     */
    public function __construct(ServiceLocatorInterface $serviceLocator, $type)
    {
        $this->message = MessagesServiceFactory::create($serviceLocator, $type, $this);
        $this->messageType = $type;
        $this->dbHelper = new DbHelper($serviceLocator);
        $this->db = $this->dbHelper->getCoreDatabase();
    }

    /**
     * @param $key
     * @return string
     */
    private function generateHashId($key)
    {
        return md5(microtime() . $key);
    }

    /**
     * @param $mode
     * @param $id
     * @return bool|string
     */
    private function setNotificationHash($mode, $id)
    {
        $hashId = $this->generateHashId($mode . $id);
        self::$notificationHash = $hashId;

        try {
            $stmt = null;
            switch ($mode) {
                case Factory::MODE_MAIL:
                    $stmt = $this->db->prepare(self::$sqlUpdHashIdMailQueue);
                    $stmt->bindValue(":placeholder", self::HASH_ID_PLACEHOLDER, \PDO::PARAM_STR);
                    $stmt->bindValue(":hashp", $hashId, \PDO::PARAM_STR);
                    break;
                case Factory::MODE_SMS:
                    $stmt = $this->db->prepare(self::$sqlUpdHashIdSmsQueue);
                    break;
                case Factory::MODE_PUSH:
                    $stmt = $this->db->prepare(self::$sqlUpdHashIdNotificationPushQueue);
                    $stmt->bindValue(":placeholder", self::HASH_ID_PLACEHOLDER, \PDO::PARAM_STR);
                    $stmt->bindValue(":hashp", $hashId, \PDO::PARAM_STR);
                    break;
                default:
                    $stmt = null;
            }

            if ($stmt !== null) {
                $stmt->bindValue(":id", $id, \PDO::PARAM_INT);
                $stmt->bindValue(":hash", $hashId, \PDO::PARAM_STR);
                return $stmt->execute();
            }
            return false;
        } catch (\PDOException $ex) {
            return $ex->getMessage();
        }
    }

    /**
     * @return null
     */
    public function getNotificationHash()
    {
        return self::$notificationHash;
    }

    /**
     * @param MessageModel $message
     * @return bool|string
     */
    public function pullMessage(Message $message)
    {
        return $this->message->pullMessage($message);
    }

    /**
     * @param array $messages
     * @return array
     */
    public function pullMessages(array $messages)
    {
        return $this->message->pullMessages($messages);
    }

    /**
     * @param $limit
     * @param $verbose
     * @param $log
     * @return bool|string
     */
    public function pushMessages($limit, $verbose, $log)
    {
        $this->logType = $this->messageType;
        $this->verboseMode = $verbose;
        $this->logMode = $log;

        if ($limit == 0) {
            $limit = $this->message->getDefaultPushLimit();
        }

        $res = $this->message->pushMessages($limit);
        return $res;
    }

    /**
     * @param $verbose
     * @param $log
     * @param $params
     * @param $from
     * @param $to
     * @param $message
     * @param $subject
     * @param null $device
     * @param string $mode
     * @return bool|\MessagesService\Service\SMSApi\Api\Response\MessageResponse
     * @throws MessagesServicePushException|MessagesServiceSmsException|MessagesServiceMailException
     */
    public function sendMessage($verbose, $log, $params, $from, $to, $message, $subject, $device = null, $mode = PushManager::ENVIRONMENT_DEV)
    {
        $this->logType = $this->messageType;
        $this->verboseMode = $verbose;
        $this->logMode = $log;

        $res = $this->message->sendMessage($params, $from, $to, $message, $subject, $device, $mode);
        return $res;
    }

    /**
     * @param $hashId
     * @return bool|string
     */
    public function removeMessageFromQueue($hashId)
    {
        $res = $this->message->removeMessageFromQueue($hashId);
        return $res;
    }

    /**
     * @param MessageModel $message
     * @return bool|string
     * @throws MessagesServiceMailException
     */
    public function pullEmailMessage(Message $message)
    {
        $res = $this->message->validateMessage($message);
        if ($res === true) {
            try {
                $stmt = $this->db->prepare(self::$sqlPullMailMsg);

                $stmt->bindValue(":type", $this->getType(), \PDO::PARAM_STR);
                $stmt->bindValue(":params", $message->getOption('jsonParams'), \PDO::PARAM_STR);
                $stmt->bindValue(":template_layout", $message->getOption('templateLayout'), \PDO::PARAM_STR);
                $stmt->bindValue(":template_params", $message->getOption('templateParams'), \PDO::PARAM_STR);
                $stmt->bindValue(":from", $message->getOption('from'), \PDO::PARAM_STR);
                $stmt->bindValue(":from_alias", $message->getOption('senderLabel'), \PDO::PARAM_STR);
                $stmt->bindValue(":reply_to", $message->getOption('replyTo'), \PDO::PARAM_STR);
                $stmt->bindValue(":reply_to_alias", $message->getOption('replyToLabel'), \PDO::PARAM_STR);
                $stmt->bindValue(":to", $message->getOption('to'), \PDO::PARAM_STR);
                $stmt->bindValue(":subject", $message->getOption('subject'), \PDO::PARAM_STR);
                $stmt->bindValue(":content", $message->getText(), \PDO::PARAM_STR);
                $stmt->bindValue(":tracking_url", $message->getOption('trackingUrl'), \PDO::PARAM_STR);
                $stmt->bindValue(":send_date", $message->getOption('sendDate'), \PDO::PARAM_STR);
                if ($stmt->execute()) {
                    return $this->setNotificationHash(Factory::MODE_MAIL, $this->db->lastInsertId());
                } else {
                    throw new MessagesServiceMailException(sprintf(MessagesServiceException::MESSAGE_MAIL_PULL, "Email insert in the queue failed"), MessagesServiceException::ERROR_MAIL_PULL_ID);
                }
            } catch (\PDOException $ex) {
                throw new MessagesServiceMailException(sprintf(MessagesServiceException::MESSAGE_MAIL_PULL, $ex->getMessage()), MessagesServiceException::ERROR_MAIL_PULL_ID);
            }
        }
        return $res;
    }

    /**
     * @param array $messages
     * @return array
     * @throws MessagesServiceMailException
     */
    public function pullEmailMessages(array $messages)
    {
        $rows = array();
        $hashIds = array();
        /** @var Message $message */
        foreach ($messages as $message) {
            $hash = $this->generateHashId($message->getOption('to'));
            $hashIds[$message->getOption('to')] = $hash;
            $trackingUrl = $message->hasOption('trackingUrl') ? str_replace(self::HASH_ID_PLACEHOLDER, $hash, $message->getOption('trackingUrl')) : null;
            $res = $this->message->validateMessage($message);
            if ($res === true) {
                $rows[] = array(
                    $hash,
                    $this->getType(),
                    $message->getOption('jsonParams'),
                    $message->getOption('templateLayout'),
                    $message->getOption('templateParams'),
                    $message->getOption('from'),
                    $message->getOption('senderLabel'),
                    $message->getOption('replyTo'),
                    $message->getOption('replyToLabel'),
                    $message->getOption('to'),
                    $message->getOption('subject'),
                    $message->getText(),
                    $trackingUrl,
                    0,
                    $message->getOption('sendDate'),
                    date("Y-m-d H:i:s"),
                    date("Y-m-d H:i:s"),
                );
            }
        }

        $row_length = count($rows[0]);
        $nb_rows = count($rows);
        $length = $nb_rows * $row_length;

        /* Fill in chunks with '?' and separate them by group of $row_length */
        $args = implode(',', array_map(
            function ($el) {
                return '(' . implode(',', $el) . ')';
            },
            array_chunk(array_fill(0, $length, '?'), $row_length)
        ));

        $params = array();
        foreach ($rows as $row) {
            foreach ($row as $value) {
                $params[] = $value;
            }
        }

        try {
            $sql = sprintf(self::$sqlPullMailMsgs, $args);
            $stmt = $this->db->prepare($sql);
            $stmt->execute($params);
        } catch (\PDOException $exception) {
            throw new MessagesServiceMailException(sprintf(MessagesServiceException::MESSAGE_MAIL, $exception->getMessage()), MessagesServiceException::ERROR_MAIL_ID);
        }

        return $hashIds;
    }

    /**
     * @param $limit
     * @return bool|string
     */
    public function pushMailMessages($limit)
    {
        $res = true;
        try {
            $stmt = $this->db->prepare(self::$sqlPushMailMsg);

            $stmt->bindValue(":type", $this->getType(), \PDO::PARAM_STR);
            $stmt->bindValue(":max_tries", $this->message->getMaxLimitsTries(), \PDO::PARAM_INT);
            $stmt->bindValue(":status", self::STATUS_SENT, \PDO::PARAM_INT);
            $stmt->bindValue(":limit", $limit, \PDO::PARAM_INT);

            if ($stmt->execute()) {
                $messages = $stmt->fetchAll(\PDO::FETCH_OBJ);
                foreach ($messages as $msg) {
                    $message = new Message($msg->content);
                    $message->setOption('jsonParams', $msg->params);
                    $message->setOption('from', $msg->from);
                    $message->setOption('senderLabel', $msg->from_alias);
                    $message->setOption('replyTo', $msg->reply_to);
                    $message->setOption('replyToLabel', $msg->reply_to_alias);
                    $message->setOption('to', $msg->to);
                    $message->setOption('subject', $msg->subject);
                    $message->setOption('trackingUrl', $msg->tracking_url);
                    $message->setOption('templateLayout', $msg->template_layout);
                    $message->setOption('templateParams', $msg->template_params);

                    try {
                        $res = $this->message->sendMailFrom($message);
                        $msg->log = "Mail sent from " . $msg->from . " to " . $msg->to;
                        $status = self::STATUS_SENT;
                    } catch (MessagesServiceMailException $ex) {
                        $res = $msg->log = $ex->getMessage();
                        $status = $this->getStatus($msg->num_tries);
                    }

                    $this->updateMailQueue($msg->id, $status, $msg->log);

                    $this->log("MAIL-SPOOLER", $this->messageToLogString($msg));
                }
            } else {
                $res = "ERROR: Query NOT EXECUTED!\n";
            }
        } catch (\PDOException $ex) {
            $res = "ERROR: " . $ex->getMessage();
        } catch (Exception $ex) {
            $res = "ERROR: " . $ex->getMessage();
        }

        return $res;
    }

    /**
     * @param $hashId
     * @param MessageModel $message
     * @return bool|string
     */
    public function updateMailMessage($hashId, Message $message)
    {
        $res = $this->message->validateMessage($message);
        if ($res === true) {
            try {
                $stmt = $this->db->prepare(self::$sqlGetMailQueueByHashId);

                $stmt->bindValue(":hash", $hashId, \PDO::PARAM_STR);

                if ($stmt->execute()) {
                    $message = $stmt->fetch(\PDO::FETCH_OBJ);
                    if ($message !== false) {
                        if ($message->status < self::STATUS_SENT) {
                            $stmt2 = $this->db->prepare(self::$sqlUpdateMailMsg);

                            $stmt2->bindValue(":params", $message->getOption('jsonParams'), \PDO::PARAM_STR);
                            $stmt2->bindValue(":template_layout", $message->getOption('templateLayout'), \PDO::PARAM_STR);
                            $stmt2->bindValue(":template_params", $message->getOption('templateParams'), \PDO::PARAM_STR);
                            $stmt2->bindValue(":from", $message->getOption('from'), \PDO::PARAM_STR);
                            $stmt2->bindValue(":from_alias", $message->getOption('senderLabel'), \PDO::PARAM_STR);
                            $stmt2->bindValue(":reply_to", $message->getOption('replyTo'), \PDO::PARAM_STR);
                            $stmt2->bindValue(":reply_to_alias", $message->getOption('replyToLabel'), \PDO::PARAM_STR);
                            $stmt2->bindValue(":to", $message->getOption('to'), \PDO::PARAM_STR);
                            $stmt2->bindValue(":subject", $message->getOption('subject'), \PDO::PARAM_STR);
                            $stmt2->bindValue(":content", $message->getText(), \PDO::PARAM_STR);
                            $stmt2->bindValue(":tracking_url", $message->getOption('trackingUrl'), \PDO::PARAM_STR);
                            $stmt2->bindValue(":send_date", $message->getOption('sendDate'), \PDO::PARAM_STR);
                            $stmt2->bindValue(":hash", $hashId, \PDO::PARAM_STR);

                            if (!$stmt2->execute()) {
                                $res = "ERROR: Email NOT UPDATED!\n";
                            }
                        }
                    }
                } else {
                    $res = "ERROR: Query NOT EXECUTED!\n";
                }
            } catch (\PDOException $ex) {
                $res = "ERROR: " . $ex->getMessage();
            }
        }
        return $res;
    }

    /**
     * @param $hashIds
     * @param $message
     * @param $subject
     * @param $sendDate
     * @return bool|string
     * @throws MessagesServiceMailException
     */
    public function updateMailMessages($hashIds, $message, $subject, $sendDate)
    {
        if (count($hashIds) > 0) {
            $ids = implode("','", $hashIds);
            $ids = "'" . $ids . "'";

            try {
                $sql = sprintf(self::$sqlUpdateMailMsgByIds, $ids);
                $stmt = $this->db->prepare($sql);

                $stmt->bindValue(":subject", $subject, \PDO::PARAM_STR);
                $stmt->bindValue(":content", $message, \PDO::PARAM_STR);
                $stmt->bindValue(":send_date", $sendDate, \PDO::PARAM_STR);

                if (!$stmt->execute()) {
                    $res = "ERROR: Email NOT UPDATED!\n";
                } else {
                    $res = true;
                }

            } catch (\PDOException $ex) {
                $res = "ERROR: " . $ex->getMessage();
            }
        } else {
            throw new MessagesServiceMailException(sprintf(MessagesServiceException::MESSAGE_MAIL, "No hash ids"), MessagesServiceException::ERROR_MAIL_ID);
        }


        return $res;
    }

    /**
     * @param $hashId
     * @return bool|string
     */
    public function removeMailFromQueue($hashId)
    {
        $res = true;

        try {
            $stmt = $this->db->prepare(self::$sqlGetMailQueueByHashId);

            $stmt->bindValue(":hash", $hashId, \PDO::PARAM_STR);

            if ($stmt->execute()) {
                $message = $stmt->fetch(\PDO::FETCH_OBJ);
                if ($message !== false) {
                    if ($message->status < self::STATUS_SENT) {
                        $stmt2 = $this->db->prepare(self::$sqlAbortSendMailQueue);
                        $stmt2->bindValue(":status", self::STATUS_ABORTED, \PDO::PARAM_INT);
                        $stmt2->bindValue(":hash", $hashId, \PDO::PARAM_STR);

                        if (!$stmt2->execute()) {
                            $res = "ERROR: Email NOT ABORTED!\n";
                        }
                    }
                }
            } else {
                $res = "ERROR: Query NOT EXECUTED!\n";
            }
        } catch (\PDOException $ex) {
            $res = "ERROR: " . $ex->getMessage();
        }

        return $res;
    }

    /**
     * @param $hashIds
     * @return bool|string
     * @throws MessagesServiceMailException
     */
    public function removeMailsFromQueue($hashIds)
    {
        if (count($hashIds) > 0) {
            $ids = implode("','", $hashIds);
            $ids = "'" . $ids . "'";

            try {
                $sql = sprintf(self::$sqlAbortSendMailsQueue, $ids);
                $stmt = $this->db->prepare($sql);

                $stmt->bindValue(":status", self::STATUS_ABORTED, \PDO::PARAM_INT);

                if (!$stmt->execute()) {
                    $res = "ERROR: MAILS NOT ABORTED!\n";
                } else {
                    $res = true;
                }

            } catch (\PDOException $ex) {
                $res = "ERROR: " . $ex->getMessage();
            }
        } else {
            throw new MessagesServiceMailException(sprintf(MessagesServiceException::MESSAGE_SMS, "No hash ids"), MessagesServiceException::ERROR_SMS_ID);
        }

        return $res;
    }

    /**
     * @param $msg_id
     * @param $status
     * @param $log
     * @return bool
     */
    private function updateMailQueue($msg_id, $status, $log)
    {
        $stmt = $this->db->prepare(self::$sqlUpdMailQueue);

        $stmt->bindValue(":id", $msg_id, \PDO::PARAM_INT);
        $stmt->bindValue(":status", $status, \PDO::PARAM_INT);
        $stmt->bindValue(":log", $log, \PDO::PARAM_STR);

        return $stmt->execute();
    }

    /**
     * @param MessageModel $message
     * @return bool|string
     * @throws MessagesServiceSmsException
     */
    public function pullSmsMessage(Message $message)
    {
        $res = $this->message->validateMessage($message);
        if ($res === true) {
            try {
                $stmt = $this->db->prepare(self::$sqlPullSmsMsg);

                $stmt->bindValue(":type", $this->getType(), \PDO::PARAM_STR);
                $stmt->bindValue(":params", $message->getOption('jsonParams'), \PDO::PARAM_STR);
                $stmt->bindValue(":from", $message->getOption('from'), \PDO::PARAM_STR);
                $stmt->bindValue(":to", $message->getOption('to'), \PDO::PARAM_STR);
                $stmt->bindValue(":content", $message->getText(), \PDO::PARAM_STR);
                $stmt->bindValue(":send_date", $message->getOption('sendDate'), \PDO::PARAM_STR);
                if ($stmt->execute()) {
                    return $this->setNotificationHash(Factory::MODE_SMS, $this->db->lastInsertId());
                } else {
                    throw new MessagesServiceSmsException(sprintf(MessagesServiceException::MESSAGE_SMS_PULL, "SMS insert in the queue failed"), MessagesServiceException::ERROR_SMS_PULL_ID);
                }
            } catch (\PDOException $ex) {
                throw new MessagesServiceSmsException(sprintf(MessagesServiceException::MESSAGE_SMS_PULL, $ex->getMessage()), MessagesServiceException::ERROR_SMS_PULL_ID);
            }
        }

        return $res;
    }

    /**
     * @param array $messages
     * @return array
     * @throws MessagesServiceSmsException
     */
    public function pullSmsMessages(array $messages)
    {
        $rows = array();
        $hashIds = array();
        /** @var Message $message */
        foreach ($messages as $message) {
            $hash = $this->generateHashId($message->getOption('to'));
            $hashIds[$message->getOption('to')] = $hash;
            $res = $this->message->validateMessage($message);
            if ($res === true) {
                $rows[] = array(
                    $hash,
                    $this->getType(),
                    $message->getOption('jsonParams'),
                    $message->getOption('from'),
                    $message->getOption('to'),
                    $message->getText(),
                    0,
                    $message->getOption('sendDate'),
                    date("Y-m-d H:i:s"),
                    date("Y-m-d H:i:s"),
                );
            }
        }

        $row_length = count($rows[0]);
        $nb_rows = count($rows);
        $length = $nb_rows * $row_length;

        /* Fill in chunks with '?' and separate them by group of $row_length */
        $args = implode(',', array_map(
            function ($el) {
                return '(' . implode(',', $el) . ')';
            },
            array_chunk(array_fill(0, $length, '?'), $row_length)
        ));

        $params = array();
        foreach ($rows as $row) {
            foreach ($row as $value) {
                $params[] = $value;
            }
        }

        try {
            $sql = sprintf(self::$sqlPullSmsMsgs, $args);
            $stmt = $this->db->prepare($sql);
            $stmt->execute($params);
        } catch (\PDOException $exception) {
            throw new MessagesServiceSmsException(sprintf(MessagesServiceException::MESSAGE_SMS, $exception->getMessage()), MessagesServiceException::ERROR_SMS_ID);
        }

        return $hashIds;
    }

    /**
     * @param $limit
     * @return bool|\MessagesService\Service\SMSApi\Api\Response\MessageResponse|string
     */
    public function pushSmsMessages($limit)
    {
        $res = true;
        try {
            $stmt = $this->db->prepare(self::$sqlPushSmsMsg);

            $stmt->bindValue(":type", $this->getType(), \PDO::PARAM_STR);
            $stmt->bindValue(":max_tries", $this->message->getMaxLimitsTries(), \PDO::PARAM_INT);
            $stmt->bindValue(":status", self::STATUS_SENT, \PDO::PARAM_INT);
            $stmt->bindValue(":limit", $limit, \PDO::PARAM_INT);

            if ($stmt->execute()) {
                $messages = $stmt->fetchAll(\PDO::FETCH_OBJ);
                foreach ($messages as $msg) {
                    $callback_id = "-";
                    $message = new Message($msg->content);
                    $message->setOption('jsonParams', $msg->params);
                    $message->setOption('from', $msg->from);
                    $message->setOption('to', $msg->to);
                    try {
                        $response = $this->message->sendSms($message);
                        if (is_a($response, 'SMSApi\Api\Response\MessageResponse')) {
                            $callback_id = $response->getId();
                            $status_msg = $this->message->getSmsapiStatusMessage($response->getStatus());
                            $msg->log = $status_msg . " from " . $msg->from . " to " . $msg->to;
                            $status = self::STATUS_SENT;
                        } else {
                            $res = $msg->log = $response;
                            $status = $this->getStatus($msg->num_tries);
                        }
                    } catch (MessagesServiceSmsException $ex) {
                        $res = $msg->log = "ERROR: " . $ex->getMessage() . " from " . $msg->from . " to " . $msg->to;
                        $status = $this->getStatus($msg->num_tries);
                    }

                    $this->updateSmsQueue($msg->id, $status, $msg->log, $callback_id);

                    $this->log("SMS-SPOOLER", $this->messageToLogString($msg));
                }
            } else {
                $res = "ERROR: Query NOT EXECUTED!\n";
            }
        } catch (\PDOException $ex) {
            $res = "ERROR: " . $ex->getMessage();
        } catch (Exception $ex) {
            $res = "ERROR: " . $ex->getMessage();
        }

        return $res;
    }

    /**
     * @param $hashId
     * @param MessageModel $message
     * @return bool|string
     */
    public function updateSmsMessage($hashId, Message $message)
    {
        $res = $this->message->validateMessage($message);
        if ($res === true) {
            try {
                $stmt = $this->db->prepare(self::$sqlGetSmsQueueByHashId);

                $stmt->bindValue(":hash", $hashId, \PDO::PARAM_STR);

                if ($stmt->execute()) {
                    $message = $stmt->fetch(\PDO::FETCH_OBJ);
                    if ($message !== false) {
                        if ($message->status < self::STATUS_SENT) {
                            $stmt2 = $this->db->prepare(self::$sqlUpdateSmsMsg);

                            $stmt2->bindValue(":params", $message->getOption('jsonParams'), \PDO::PARAM_STR);
                            $stmt2->bindValue(":from", $message->getOption('from'), \PDO::PARAM_STR);
                            $stmt2->bindValue(":to", $message->getOption('to'), \PDO::PARAM_STR);
                            $stmt2->bindValue(":content", $message->getText(), \PDO::PARAM_STR);
                            $stmt2->bindValue(":send_date", $message->getOption('sendDate'), \PDO::PARAM_STR);
                            $stmt2->bindValue(":hash", $hashId, \PDO::PARAM_STR);

                            if (!$stmt2->execute()) {
                                $res = "ERROR: SMS NOT UPDATED!\n";
                            }
                        }
                    }
                } else {
                    $res = "ERROR: Query NOT EXECUTED!\n";
                }
            } catch (\PDOException $ex) {
                $res = "ERROR: " . $ex->getMessage();
            }
        }
        return $res;
    }

    /**
     * @param $hashIds
     * @param $message
     * @param $sendDate
     * @return bool|string
     * @throws MessagesServiceSmsException
     */
    public function updateSmsMessages($hashIds, $message, $sendDate)
    {
        if (count($hashIds) > 0) {
            $ids = implode("','", $hashIds);
            $ids = "'" . $ids . "'";

            try {
                $sql = sprintf(self::$sqlUpdateSmsMsgByIds, $ids);
                $stmt = $this->db->prepare($sql);

                $stmt->bindValue(":content", $message, \PDO::PARAM_STR);
                $stmt->bindValue(":send_date", $sendDate, \PDO::PARAM_STR);

                if (!$stmt->execute()) {
                    $res = "ERROR: SMS NOT UPDATED!\n";
                } else {
                    $res = true;
                }

            } catch (\PDOException $ex) {
                $res = "ERROR: " . $ex->getMessage();
            }
        } else {
            throw new MessagesServiceSmsException(sprintf(MessagesServiceException::MESSAGE_SMS, "No hash ids"), MessagesServiceException::ERROR_SMS_ID);
        }

        return $res;
    }

    /**
     * @param $hashId
     * @return bool|string
     */
    public function removeSmsFromQueue($hashId)
    {
        $res = true;

        try {
            $stmt = $this->db->prepare(self::$sqlGetSmsQueueByHashId);

            $stmt->bindValue(":hash", $hashId, \PDO::PARAM_STR);

            if ($stmt->execute()) {
                $message = $stmt->fetch(\PDO::FETCH_OBJ);
                if ($message !== false) {
                    if ($message->status < self::STATUS_SENT) {
                        $stmt2 = $this->db->prepare(self::$sqlAbortSendSmsQueue);
                        $stmt2->bindValue(":status", self::STATUS_ABORTED, \PDO::PARAM_INT);
                        $stmt2->bindValue(":hash", $hashId, \PDO::PARAM_STR);

                        if (!$stmt2->execute()) {
                            $res = "ERROR: Email NOT ABORTED!\n";
                        }
                    }
                }
            } else {
                $res = "ERROR: Query NOT EXECUTED!\n";
            }
        } catch (\PDOException $ex) {
            $res = "ERROR: " . $ex->getMessage();
        }

        return $res;
    }

    /**
     * @param $hashIds
     * @return bool|string
     * @throws MessagesServiceSmsException
     */
    public function removeSmssFromQueue($hashIds)
    {
        if (count($hashIds) > 0) {
            $ids = implode("','", $hashIds);
            $ids = "'" . $ids . "'";

            try {
                $sql = sprintf(self::$sqlAbortSendSmssQueue, $ids);
                $stmt = $this->db->prepare($sql);

                $stmt->bindValue(":status", self::STATUS_ABORTED, \PDO::PARAM_INT);

                if (!$stmt->execute()) {
                    $res = "ERROR: SMS NOT ABORTED!\n";
                } else {
                    $res = true;
                }

            } catch (\PDOException $ex) {
                $res = "ERROR: " . $ex->getMessage();
            }
        } else {
            throw new MessagesServiceSmsException(sprintf(MessagesServiceException::MESSAGE_SMS, "No hash ids"), MessagesServiceException::ERROR_SMS_ID);
        }

        return $res;
    }

    /**
     * @param $msg_id
     * @param $status
     * @param $log
     * @param $callback_id
     * @return bool
     */
    private function updateSmsQueue($msg_id, $status, $log, $callback_id)
    {
        $stmt = $this->db->prepare(self::$sqlUpdSmsQueue);

        $stmt->bindValue(":id", $msg_id, \PDO::PARAM_INT);
        $stmt->bindValue(":status", $status, \PDO::PARAM_INT);
        $stmt->bindValue(":log", $log, \PDO::PARAM_STR);
        $stmt->bindValue(":callback_id", $callback_id, \PDO::PARAM_STR);

        return $stmt->execute();
    }

    /**
     * @param MessageModel $message
     * @return bool|string
     * @throws MessagesServicePushException
     */
    public function pullNotificationPushMessage(Message $message)
    {
        $res = $this->message->validateMessage($message);
        if ($res === true) {
            try {
                $stmt = $this->db->prepare(self::$sqlPullPushNotificationMsg);

                $stmt->bindValue(":type", $message->getOption('device'), \PDO::PARAM_STR);
                $stmt->bindValue(":params", $message->getOption('jsonParams'), \PDO::PARAM_STR);
                $stmt->bindValue(":mode", $message->getOption('mode'), \PDO::PARAM_STR);
                $stmt->bindValue(":title", $message->getOption('title'), \PDO::PARAM_STR);
                $stmt->bindValue(":to", $message->getOption('to'), \PDO::PARAM_STR);
                $stmt->bindValue(":content", $message->getText(), \PDO::PARAM_STR);
                $stmt->bindValue(":tracking_url", $message->getOption('trackingUrl'), \PDO::PARAM_STR);
                $stmt->bindValue(":collapse_id", $message->getOption('collapseId'), \PDO::PARAM_STR);
                $stmt->bindValue(":send_date", $message->getOption('sendDate'), \PDO::PARAM_STR);

                if ($stmt->execute()) {
                    return $this->setNotificationHash(Factory::MODE_PUSH, $this->db->lastInsertId());
                } else {
                    throw new MessagesServicePushException(sprintf(MessagesServiceException::MESSAGE_PUSH_PULL, "Push message insert in the queue failed"), MessagesServiceException::ERROR_PUSH_PULL_ID);
                }
            } catch (\PDOException $ex) {
                throw new MessagesServicePushException(sprintf(MessagesServiceException::MESSAGE_PUSH_PULL, $ex->getMessage()), MessagesServiceException::ERROR_PUSH_PULL_ID);
            }
        }

        return $res;
    }

    /**
     * @param array $messages
     * @return array
     * @throws MessagesServicePushException
     */
    public function pullNotificationPushMessages(array $messages)
    {
        $rows = array();
        $hashIds = array();
        /** @var Message $message */
        foreach ($messages as $message) {
            $hash = $this->generateHashId($message->getOption('to'));
            $hashIds[$message->getOption('to')] = $hash;
            $trackingUrl = $message->hasOption('trackingUrl') ? str_replace(self::HASH_ID_PLACEHOLDER, $hash, $message->getOption('trackingUrl')) : null;
            $res = $this->message->validateMessage($message);
            if ($res === true) {
                $rows[] = array(
                    $hash,
                    $message->getOption('device'),
                    $message->getOption('jsonParams'),
                    $message->getOption('mode'),
                    $message->getOption('title'),
                    $message->getOption('to'),
                    $message->getText(),
                    $trackingUrl,
                    $message->getOption('collapseId'),
                    0,
                    $message->getOption('sendDate'),
                    date("Y-m-d H:i:s"),
                    date("Y-m-d H:i:s"),
                );
            }
        }

        $row_length = count($rows[0]);
        $nb_rows = count($rows);
        $length = $nb_rows * $row_length;

        /* Fill in chunks with '?' and separate them by group of $row_length */
        $args = implode(',', array_map(
            function ($el) {
                return '(' . implode(',', $el) . ')';
            },
            array_chunk(array_fill(0, $length, '?'), $row_length)
        ));

        $params = array();
        foreach ($rows as $row) {
            foreach ($row as $value) {
                $params[] = $value;
            }
        }

        try {
            $sql = sprintf(self::$sqlPullPushNotificationMsgs, $args);
            $stmt = $this->db->prepare($sql);
            $stmt->execute($params);
        } catch (\PDOException $exception) {
            throw new MessagesServicePushException(sprintf(MessagesServiceException::MESSAGE_PUSH, $exception->getMessage()), MessagesServiceException::ERROR_PUSH_ID);
        }

        return $hashIds;
    }

    /**
     * @param $limit
     * @return bool|string
     */
    public function pushNotificationPushMessages($limit)
    {
        $res = true;
        try {
            $stmt = $this->db->prepare(self::$sqlPushNotificationPushMsg);

            $stmt->bindValue(":max_tries", $this->message->getMaxLimitsTries(), \PDO::PARAM_INT);
            $stmt->bindValue(":status", self::STATUS_SENT, \PDO::PARAM_INT);
            $stmt->bindValue(":limit", $limit, \PDO::PARAM_INT);

            if ($stmt->execute()) {
                $messages = $stmt->fetchAll(\PDO::FETCH_OBJ);
                foreach ($messages as $msg) {
                    try {
                        $message = new Message($msg->content);
                        $message->setOption('jsonParams', $msg->params);
                        $message->setOption('device', $msg->type);
                        $message->setOption('to', $msg->to);
                        $message->setOption('mode', $msg->mode);
                        $message->setOption('title', $msg->title);
                        $message->setOption('collapseId', $msg->collapse_id);
                        $message->setOption('trackingUrl', $msg->tracking_url);
                        $message->setOption('hashId', $msg->hash_id);

                        try {
                            $res = $this->message->sendNotificationPush($message);
                            if ($res === true) {
                                $msg->log = "Push notification sent to " . $msg->to;
                                $status = self::STATUS_SENT;
                            } else {
                                $msg->log = $res;
                                $status = self::STATUS_UNINSTALLED;
                            }
                        } catch (MessagesServicePushException $ex) {
                            $res = $msg->log = $ex->getMessage();
                            $status = $this->getStatus($msg->num_tries);
                        }
                    } catch (AdapterException $ex) {
                        $res = $msg->log = "ERROR - AdapterException: " . $ex->getMessage();
                        $status = $this->getStatus($msg->num_tries);
                    } catch (PushException $ex) {
                        $res = $msg->log = "ERROR - PushException: " . $ex->getMessage();
                        $status = $this->getStatus($msg->num_tries);
                    } catch (\ZendService\Google\Exception\InvalidArgumentException $ex) {
                        $res = $msg->log = "ERROR - InvalidArgumentException: " . $ex->getMessage();
                        $status = $this->getStatus($msg->num_tries);
                    } catch (\ZendService\Google\Exception\RuntimeException $ex) {
                        $res = $msg->log = "ERROR - RuntimeException: " . $ex->getMessage();
                        $status = $this->getStatus($msg->num_tries);
                    } catch (\ZendService\Apple\Exception\InvalidArgumentException $ex) {
                        $res = $msg->log = "ERROR - InvalidArgumentException: " . $ex->getMessage();
                        $status = $this->getStatus($msg->num_tries);
                    } catch (\ZendService\Apple\Exception\RuntimeException $ex) {
                        $res = $msg->log = "ERROR - RuntimeException: " . $ex->getMessage();
                        $status = $this->getStatus($msg->num_tries);
                    } catch (\ZendService\Apple\Exception\StreamSocketClientException $ex) {
                        $res = $msg->log = "ERROR - StreamSocketClientException: " . $ex->getMessage();
                        $status = $this->getStatus($msg->num_tries);
                    }

                    $this->updateNotificationPushQueue($msg->id, $status, $msg->log);

                    $this->log("PUSHMESSAGE-SPOOLER", $this->messageToLogString($msg));
                }
            } else {
                $res = "ERROR: Query NOT EXECUTED!\n";
            }
        } catch (\PDOException $ex) {
            $res = "ERROR - PDOException: " . $ex->getMessage();
        } catch (Exception $ex) {
            $res = "ERROR - Exception: " . $ex->getMessage();
        }

        return $res;
    }

    /**
     * @param $hashId
     * @param MessageModel $message
     * @return bool|string
     */
    public function updatePushMessage($hashId, Message $message)
    {
        $res = $this->message->validateMessage($message);
        if ($res === true) {
            try {
                $stmt = $this->db->prepare(self::$sqlGetNotificationPushQueueByHashId);

                $stmt->bindValue(":hash", $hashId, \PDO::PARAM_STR);

                if ($stmt->execute()) {
                    $message = $stmt->fetch(\PDO::FETCH_OBJ);
                    if ($message !== false) {
                        if ($message->status < self::STATUS_SENT) {
                            $stmt2 = $this->db->prepare(self::$sqlUpdatePushMsg);

                            $stmt2->bindValue(":params", $message->getOption('jsonParams'), \PDO::PARAM_STR);
                            $stmt2->bindValue(":mode", $message->getOption('mode'), \PDO::PARAM_STR);
                            $stmt2->bindValue(":title", $message->getOption('title'), \PDO::PARAM_STR);
                            $stmt2->bindValue(":to", $message->getOption('to'), \PDO::PARAM_STR);
                            $stmt2->bindValue(":content", $message->getText(), \PDO::PARAM_STR);
                            $stmt2->bindValue(":tracking_url", $message->getOption('trackingUrl'), \PDO::PARAM_STR);
                            $stmt2->bindValue(":collapse_id", $message->getOption('collapseId'), \PDO::PARAM_STR);
                            $stmt2->bindValue(":send_date", $message->getOption('sendDate'), \PDO::PARAM_STR);
                            $stmt2->bindValue(":hash", $hashId, \PDO::PARAM_STR);

                            if (!$stmt2->execute()) {
                                $res = "ERROR: Notification Push Message NOT UPDATED!\n";
                            }
                        }
                    }
                } else {
                    $res = "ERROR: Query NOT EXECUTED!\n";
                }
            } catch (\PDOException $ex) {
                $res = "ERROR: " . $ex->getMessage();
            }
        }
        return $res;
    }

    /**
     * @param $hashIds
     * @param $message
     * @param $subject
     * @param $sendDate
     * @return bool|string
     * @throws MessagesServicePushException
     */
    public function updatePushMessages($hashIds, $message, $subject, $sendDate)
    {
        if (count($hashIds) > 0) {
            $ids = implode("','", $hashIds);
            $ids = "'" . $ids . "'";

            try {
                $sql = sprintf(self::$sqlUpdatePushMsgByIds, $ids);
                $stmt = $this->db->prepare($sql);

                $stmt->bindValue(":title", $subject, \PDO::PARAM_STR);
                $stmt->bindValue(":content", $message, \PDO::PARAM_STR);
                $stmt->bindValue(":send_date", $sendDate, \PDO::PARAM_STR);

                if (!$stmt->execute()) {
                    $res = "ERROR: Notification Push Message NOT UPDATED!\n";
                } else {
                    $res = true;
                }

            } catch (\PDOException $ex) {
                $res = "ERROR: " . $ex->getMessage();
            }
        } else {
            throw new MessagesServicePushException(sprintf(MessagesServiceException::MESSAGE_PUSH, "No hash ids"), MessagesServiceException::ERROR_PUSH_ID);
        }

        return $res;
    }

    /**
     * @param $hashId
     * @return bool|string
     */
    public function removePushMessageFromQueue($hashId)
    {
        $res = true;

        try {
            $stmt = $this->db->prepare(self::$sqlGetNotificationPushQueueByHashId);

            $stmt->bindValue(":hash", $hashId, \PDO::PARAM_STR);

            if ($stmt->execute()) {
                $message = $stmt->fetch(\PDO::FETCH_OBJ);
                if ($message !== false) {
                    if ($message->status < self::STATUS_SENT) {
                        $stmt2 = $this->db->prepare(self::$sqlAbortSendNotificationPushQueue);
                        $stmt2->bindValue(":status", self::STATUS_ABORTED, \PDO::PARAM_INT);
                        $stmt2->bindValue(":hash", $hashId, \PDO::PARAM_STR);

                        if (!$stmt2->execute()) {
                            $res = "ERROR: Email NOT ABORTED!\n";
                        }
                    }
                }
            } else {
                $res = "ERROR: Query NOT EXECUTED!\n";
            }
        } catch (\PDOException $ex) {
            $res = "ERROR: " . $ex->getMessage();
        }

        return $res;
    }

    /**
     * @param $hashIds
     * @return bool|string
     * @throws MessagesServicePushException
     */
    public function removePushMessagesFromQueue($hashIds)
    {
        if (count($hashIds) > 0) {
            $ids = implode("','", $hashIds);
            $ids = "'" . $ids . "'";

            try {
                $sql = sprintf(self::$sqlAbortSendNotificationsPushQueue, $ids);
                $stmt = $this->db->prepare($sql);

                $stmt->bindValue(":status", self::STATUS_ABORTED, \PDO::PARAM_INT);

                if (!$stmt->execute()) {
                    $res = "ERROR: PUSH NOTIFICATIONS NOT ABORTED!\n";
                } else {
                    $res = true;
                }

            } catch (\PDOException $ex) {
                $res = "ERROR: " . $ex->getMessage();
            }
        } else {
            throw new MessagesServicePushException(sprintf(MessagesServiceException::MESSAGE_SMS, "No hash ids"), MessagesServiceException::ERROR_SMS_ID);
        }

        return $res;
    }

    /**
     * @param $token
     * @return bool
     * @throws Exception
     */
    public function isAppInstalled($token)
    {
        if ($this->message->getType() === MessagesServiceFactory::MODE_PUSH) {

            $stmt = $this->db->prepare(self::$sqlGetNotificationPushValidToken);

            $stmt->bindValue(":to", $token, \PDO::PARAM_STR);

            if ($stmt->execute()) {
                $count = $stmt->rowCount();
                if ($count == 0) {
                    return true;
                } else {
                    $message = $stmt->fetch(\PDO::FETCH_OBJ);
                    if ($message->status == self::STATUS_UNINSTALLED) {
                        return false;
                    }
                }
            }
        } else {
            throw new Exception("Method not allowed.");
        }
        return true;
    }

    /**
     * @param $msg_id
     * @param $status
     * @param $log
     * @return bool
     */
    private function updateNotificationPushQueue($msg_id, $status, $log)
    {
        $stmt = $this->db->prepare(self::$sqlUpdNotificationPushQueue);

        $stmt->bindValue(":id", $msg_id, \PDO::PARAM_INT);
        $stmt->bindValue(":status", $status, \PDO::PARAM_INT);
        $stmt->bindValue(":log", $log, \PDO::PARAM_STR);

        return $stmt->execute();
    }

    /**
     * Return TRUE if logger will show log in console / page, else return FALSE
     * @return bool
     */
    public function showLog()
    {
        return $this->verboseMode;
    }

    /**
     * @return null
     */
    public function getType()
    {
        return $this->messageType;
    }

    /**
     * Return complete file path if logger will wrote log on file. else return NULL
     * @return null|string
     */
    public function getLogFile()
    {
        $logFile = null;
        if ($this->logMode) {
            $now = new \DateTime('now');

            $logFile = sprintf(
                $this->message->getLogPattern(),
                $now->format('Y'),
                $now->format('m'),
                $now->format('d')
            );
        }

        return $logFile;
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function getPhoneCountries()
    {
        if ($this->message->getType() === MessagesServiceFactory::MODE_SMS) {
            return $this->message->getPhoneCountries();
        } else {
            throw new Exception("Method not allowed.");
        }
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getDefaultPhoneCode()
    {
        $user_lang = explode(",", $_SERVER['HTTP_ACCEPT_LANGUAGE'])[0];
        $transformed = preg_replace('/\-/', '_', $user_lang);

        $rows = $this->getPhoneCountries();

        $default_number = (isset($rows[$transformed])) ? $rows[$transformed] : '+39';

        return $default_number;
    }

    /**
     * @param $msg
     * @return string
     */
    private function messageToLogString($msg)
    {
        return sprintf(
            $this->log_format,
            $msg->id,
            $this->statusToString($msg->status),
            ($msg->num_tries + 1),
            $this->message->getMaxLimitsTries(),
            $msg->log
        );
    }

    /**
     * @param $status
     * @return string
     */
    private function statusToString($status)
    {
        switch ($status) {
            case self::STATUS_READY:
                $res = "READY";
                break;
            case self::STATUS_RETRY:
                $res = "RETRY";
                break;
            case self::STATUS_SENT:
                $res = "SENT";
                break;
            case self::STATUS_FAIL:
                $res = "FAIL";
                break;
            case self::STATUS_ABORTED:
                $res = "ABORTED";
                break;
            case self::STATUS_UNINSTALLED:
                $res = "UNINSTALLED";
                break;

            default:
                $res = "UNKNOWN";
                break;
        }

        return $res;
    }

    /**
     * @param $num_tries
     * @return int
     */
    private function getStatus($num_tries)
    {
        if ($num_tries == $this->message->getMaxLimitsTries() - 1) {
            $status = self::STATUS_FAIL;
        } else {
            $status = self::STATUS_RETRY;
        }
        return $status;
    }

    /**
     * @param $messageText
     * @param array $params
     * @return MessageModel
     * @throws Exception
     */
    public function createMessage($messageText, array $params)
    {
        return new Message($messageText, $params);
    }
}
