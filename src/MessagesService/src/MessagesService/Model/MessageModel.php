<?php
/**
 * Created by PhpStorm.
 * User: serpe
 * Date: 20/01/17
 * Time: 16:00
 */
namespace MessagesService\Model;

use Exception;

class MessageModel extends BaseMessageModel
{
    /**
     * @var string
     */
    private $text;

    /**
     * MessageModel constructor.
     * @param $text
     * @param array $options
     * @throws Exception
     */
    public function __construct($text, array $options = [])
    {
        if ($text == "") {
            throw new Exception("Text message is empty");
        }
        $this->text    = $text;
        $this->options = $options;
    }

    /**
     * Get Text.
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set Text.
     *
     * @param string $text Text
     *
     * @return $this
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }
}