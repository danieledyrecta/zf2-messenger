<?php
namespace MessagesService\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use MessagesService\Model\MessagesModel;
use MessagesService\Model\InstallModel;

class ConsoleController extends AbstractActionController
{
    const COMMAND_INSTALL = "install";
    const COMMAND_UPDATE = "update";

    private static $validCommand = array(self::COMMAND_INSTALL, self::COMMAND_UPDATE);

    /* @var $route_params RouteMatch */
    private $route_params = null;

    private $verboseMode = false;
    private $debugMode = false;
    private $logMode = false;

    protected $config = null;

    public function onDispatch(MvcEvent $e)
    {
        $request = $this->getRequest();

        if (!$request instanceof \Zend\Console\Request) {
            die('ACCESS DANIED: You can only use this action from a console!');
        }

        $this->route_params = $this->getEvent()->getRouteMatch();
        $this->verboseMode = ($this->route_params->getParam('verbose') || $this->route_params->getParam('v'));
        $this->debugMode = ($this->route_params->getParam('debug') || $this->route_params->getParam('d'));
        $this->logMode = ($this->route_params->getParam('log') || $this->route_params->getParam('l'));

        if ($this->debugMode) echo print_r($this->route_params->getParams(), true) . "\n";

        return parent::onDispatch($e);
    }

    public function moduleInstallAction()
    {
        $command = strtolower($this->route_params->getParam('command'));

        if ($this->isValidCommand($command)) {
            $installModel = new InstallModel(@$this->getServiceLocator(), $this->verboseMode, $this->logMode);
            $start_time = time();
            switch (strtolower($command)) {
                case self::COMMAND_INSTALL:
                    echo "\nInstalling zf2-messenger module...\n";
                    $res = $installModel->createDbTables();
                    if ($res === true) {
                        echo "zf2-messenger module installed successfully\n";
                    } else {
                        if ($res !== false) {
                            echo $res . "\n";
                        } else {
                            echo "An error occurred during zf2-messenger module installation\n";
                        }
                    }
                    break;
                case self::COMMAND_UPDATE:
                    echo "\nUpdating zf2-messenger module...\n";
                    $res = $installModel->updateDbTables();
                    if ($res === true) {
                        echo "zf2-messenger module updated successfully\n";
                    } else {
                        if ($res !== false) {
                            echo $res . "\n";
                        } else {
                            echo "An error occurred during zf2-messenger module updating\n";
                        }
                    }
                    break;
            }
            $end_time = time();
            echo "[" . ($end_time - $start_time) . "] seconds elapsed\n\n";
        } else {
            echo "\nUnknown command\n\n";
        }

    }

    public function testAction()
    {
        $msg = $this->route_params->getParam('msg');
        echo "test rhubbit messages " . $msg . "\n";
    }

    public function sendAction()
    {
        $type = strtolower($this->route_params->getParam('type'));
        $limit = (int)$this->route_params->getParam('limit');

        $msgModel = new MessagesModel(@$this->getServiceLocator(), $type);

        if ($this->verboseMode) echo "\n";

        $result = $msgModel->pushMessages($limit, $this->verboseMode, $this->logMode);

        if ($result === true) {
            $msgModel->log("CONSOLE-CONTROLLER", sprintf("TASK COMPLETE: %s task sent messages with success.", strtoupper($type)));
        } else {
            $msgModel->log("CONSOLE-CONTROLLER", $result);
        }

        if ($this->verboseMode) echo "\n\n";
    }

    public function sendWithoutQueueAction()
    {
        $type = strtolower($this->route_params->getParam('type'));
        $to = $this->route_params->getParam('to');
        $from = $this->route_params->getParam('from');
        $params = $this->route_params->getParam('params');
        $msg = $this->route_params->getParam('msg');
        $subject = $this->route_params->getParam('subject');

        $msgModel = new MessagesModel(@$this->getServiceLocator(), $type);

        if ($this->verboseMode) echo "\n";

        $result = $msgModel->sendMessage($this->verboseMode, $this->logMode, $params, $from, $to, $msg, $subject);

        if ($result === true) {
            $msgModel->log("CONSOLE-CONTROLLER", sprintf("TASK COMPLETE: %s to %s sent with success.", strtoupper($type), $to));
        } else {
            $msgModel->log("CONSOLE-CONTROLLER", $result);
        }


        if ($this->verboseMode) echo "\n\n";
    }

    private function isValidCommand($command)
    {
        return in_array(strtolower($command), self::$validCommand);
    }

}
