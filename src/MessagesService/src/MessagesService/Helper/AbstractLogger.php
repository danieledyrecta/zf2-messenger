<?php

namespace MessagesService\Helper;

abstract class AbstractLogger
{
    /**
     * Return TRUE if logger will show log in console / page, else return FALSE
     * @return bool
     */
    public abstract function showLog();

    /**
     * Return complete file path if logger will wrote log on file. else return NULL
     * @return null|string
     */
    public abstract function getLogFile();

    /**
     * Log text message received in input showing it in console / page if showLog return TRUE
     * and writing it on file if getLogFile is not NULL
     *
     * @param $tag string added as log label
     * @param $message string message to log
     */
    public function log($tag, $message) {
        $bt = debug_backtrace();
        $caller = array_shift($bt);
        $phpFile = $caller['file'];
        $phpLine = $caller['line'];

        $now = \DateTime::createFromFormat('U.u', number_format(microtime(true), 6, '.', ''));

        $logMessage = $now->format("Y-m-d H:i:s.u") . " | " . $tag . " | " . $message . " | " . $phpFile." (".$phpLine.")\n";

        if($this->showLog()) echo $logMessage;

        $logFile = $this->getLogFile();

        if($logFile != null) {
            $dirpath = dirname($logFile);

            if($this->makedirs($dirpath, 0755)) {
                $file = fopen($logFile, "a") or die("ERROR (AbstractLogger): Unable to open file!\n");

                fwrite($file, $logMessage);
                fclose($file);
            } else {
                die("ERROR (AbstractLogger): Unable to create log folder ($dirpath)!\n");
            }
        }
    }

    private function makedirs($dirpath, $mode=0777) {
        return is_dir($dirpath) || mkdir($dirpath, $mode, true);
    }
}