<?php
namespace MessagesService\Service;

use MessagesService\Exception\MessagesServiceException,
    MessagesService\Exception\MessagesServiceMailException;
use MessagesService\Model\MessagesModel,
    MessagesService\Model\MessageModel as Message;
use MessagesService\Event\MailListener;
use Zend\ServiceManager\ServiceLocatorInterface;
use AcMailer\Service\MailService;

class EmailService implements IMessagesService
{
    private static $instance = null;
    private $config;
    private $type;

    protected $serviceLocator;
    protected $messagesModel = null;
    protected $mailListener = null;

    /**
     * EmailService constructor.
     * @param $serviceLocator
     * @param $config
     * @param MessagesModel $messagesModel
     */
    private function __construct(ServiceLocatorInterface $serviceLocator, $config, MessagesModel $messagesModel)
    {
        $this->config = $config;
        $this->serviceLocator = $serviceLocator;
        $this->messagesModel = $messagesModel;
        $this->type = Factory::MODE_MAIL;
    }

    /**
     * @param $serviceLocator
     * @param MessagesModel $config
     * @param MessagesModel $messagesModel
     * @return EmailService|null
     */
    public static function getInstance(ServiceLocatorInterface $serviceLocator, $config, MessagesModel $messagesModel)
    {
        if (self::$instance == null) {
            self::$instance = new EmailService($serviceLocator, $config, $messagesModel);
        }
        return self::$instance;
    }

    public function getDefaultPushLimit()
    {
        return $this->config['limit.push.default'];
    }

    public function getMaxLimitsTries()
    {
        return $this->config['limit.max.tries'];
    }

    public function getSenderLabel()
    {
        return $this->config['sender.label'];
    }

    /**
     * Send an email immediately
     *
     * @param $params
     * @param $from
     * @param $to
     * @param $messageText
     * @param string $subject
     * @return bool
     * @throws MessagesServiceMailException
     * @throws \Exception
     */
    public function sendMessage($params, $from, $to, $messageText, $subject = "")
    {
        $message = new Message($messageText);
        $message->setOption('jsonParams', $params);
        $message->setOption('from', $from);
        $message->setOption('to', $to);
        $message->setOption('subject', $subject);

        $res = $this->validateMessage($message);
        if ($res === true) {
            return $this->sendMailFrom($message);
        }
    }

    /**
     * Validate an email message
     *
     * @param Message $message
     * @return bool
     * @throws MessagesServiceMailException
     */
    public function validateMessage(Message $message)
    {
        // Mandatory params
        if (!$message->hasOption('jsonParams')) {
            throw new MessagesServiceMailException(MessagesServiceException::MESSAGE_MAIL_JSON_MISSING, MessagesServiceException::ERROR_MAIL_JSON_MISSING_ID);
        } else {
            $jsonParams = json_decode($message->getOption('jsonParams'));
            if ($jsonParams === null) {
                // Set params from application config
                $smtpConfig = $this->serviceLocator->get("config")["acmailer_options"]["default"]["smtp_options"];
                $emailParams = new \stdClass();
                $emailParams->username = $smtpConfig["connection_config"]["username"];
                $emailParams->password = $smtpConfig["connection_config"]["password"];
                $emailParams->authentication = $smtpConfig["connection_class"];
                $emailParams->host = $smtpConfig["host"];
                $emailParams->port = $smtpConfig["port"];
                $emailParams->ssl = $smtpConfig["connection_config"]["ssl"];
                $message->setOption('jsonParams', json_encode($emailParams));
            } elseif (
                !property_exists($jsonParams, 'username') ||
                !property_exists($jsonParams, 'password') ||
                !property_exists($jsonParams, 'authentication') ||
                !property_exists($jsonParams, 'host') ||
                !property_exists($jsonParams, 'port') ||
                !property_exists($jsonParams, 'ssl')
            ) {
                throw new MessagesServiceMailException(MessagesServiceException::MESSAGE_MAIL_JSON_VALIDATOR, MessagesServiceException::ERROR_MAIL_JSON_VALIDATOR_ID);
            }
        }
        if (!$message->hasOption('from')) {
            throw new MessagesServiceMailException(MessagesServiceException::MESSAGE_MAIL_FROM_MISSING, MessagesServiceException::ERROR_MAIL_FROM_MISSING_ID);
        } else {
            $email = $message->getOption('from');
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                throw new MessagesServiceMailException(sprintf(MessagesServiceException::MESSAGE_MAIL_FROM_VALIDATOR, $email), MessagesServiceException::ERROR_MAIL_FROM_VALIDATOR_ID);
            }
        }
        if (!$message->hasOption('to')) {
            throw new MessagesServiceMailException(MessagesServiceException::MESSAGE_MAIL_TO_MISSING, MessagesServiceException::ERROR_MAIL_TO_MISSING_ID);
        } else {
            $email = $message->getOption('to');
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                throw new MessagesServiceMailException(sprintf(MessagesServiceException::MESSAGE_MAIL_TO_VALIDATOR, $email), MessagesServiceException::ERROR_MAIL_TO_VALIDATOR_ID);
            }
        }

        // Optional params
        if (!$message->hasOption('subject')) {
            $message->setOption('subject', "");
        }
        if (!$message->hasOption('templateLayout')) {
            $message->setOption('templateLayout', null);
        }
        if (!$message->hasOption('senderLabel')) {
            $message->setOption('senderLabel', null);
        }
        if (!$message->hasOption('replyTo')) {
            $message->setOption('replyTo', null);
        }
        if (!$message->hasOption('replyToLabel')) {
            $message->setOption('replyToLabel', null);
        }
        if (!$message->hasOption('sendDate')) {
            $message->setOption('sendDate', date("Y-m-d H:i:s"));
        }
        if (!$message->hasOption('templateParams')) {
            $message->setOption('templateParams', null);
        } else {
            $jsonTemplateParams = json_decode($message->getOption('templateParams'));
            if ($jsonTemplateParams === null) {
                throw new MessagesServiceMailException(MessagesServiceException::MESSAGE_MAIL_JSON_VALIDATOR, MessagesServiceException::ERROR_MAIL_JSON_VALIDATOR_ID);
            }
        }
        if (!$message->hasOption('trackingUrl')) {
            $message->setOption('trackingUrl', null);
        }

        return true;
    }

    /**
     * Send an email
     *
     * @param Message $message
     * @return bool
     * @throws MessagesServiceMailException
     */
    public function sendMailFrom(Message $message)
    {
        $params = json_decode($message->getOption('jsonParams'));
        $mailService = $this->getMailService($message->getOption('jsonParams'));

        if ($mailService !== null) {
            $senderLabel = $message->getOption('senderLabel') == null ? $this->config['sender.label'] : $message->getOption('senderLabel');
            $mailService->getMessage()
                ->setSubject($message->getOption('subject'))
                ->addFrom($message->getOption('from'), $senderLabel);

            $messageTemplate = $message->getOption('templateLayout') == null ? $this->config['defaultTemplate'] : $message->getOption('templateLayout');
            $paramsTemplate = $message->getOption('templateParams') !== null ? json_decode($message->getOption('templateParams'), true) : array();
            $trackingUrl = $message->getOption('trackingUrl');
            $paramsTemplate['content'] = $message->getText();
            $paramsTemplate['trackingUrl'] = $trackingUrl;
            $mailService->setTemplate($messageTemplate, $paramsTemplate);
            $mailService->getMessage()->setTo($message->getOption('to'));

            if ($message->getOption('replyTo') !== null) {
                $replyToLabel = $message->getOption('replyToLabel');
                $mailService->getMessage()
                    ->setReplyTo($message->getOption('replyTo'), $replyToLabel);
            }

            $result = $mailService->send();

            if ($result->isValid()) {
                if (property_exists($params, "callback")) {
                    $getParam = array(
                        "type" => $this->getType(),
                        "hash_id" => $message->hasOption("hashId") ? $message->getOption("hashId") : null,
                        "status" => "success");

                    $HttpClient = new Client();
                    $HttpClient->setUri($params->callback);
                    $HttpClient->setOptions(array(
                        'maxredirects' => 0,
                        'timeout' => 5,));
                    $HttpClient->setMethod("GET");
                    $HttpClient->setParameterGet($getParam);
                    $HttpClient->send();
                    $HttpClient->getAdapter()->close();
                }
                return true;
            } else {
                $msg = sprintf(MessagesServiceException::MESSAGE_MAIL, $result->getMessage());
                throw new MessagesServiceMailException($msg, MessagesServiceException::ERROR_MAIL_ID);
            }
        } else {
            throw new MessagesServiceMailException(MessagesServiceException::MESSAGE_MAIL_SMTP_VALIDATOR, MessagesServiceException::ERROR_MAIL_SMTP_VALIDATOR_ID);
        }
    }

    /**
     * @param Message $message
     * @return bool|string
     * @throws MessagesServiceMailException
     */
    public function pullMessage(Message $message)
    {
        return $this->messagesModel->pullEmailMessage($message);
    }

    /**
     * @param array $messages
     * @return array
     * @throws MessagesServiceMailException
     */
    public function pullMessages(array $messages)
    {
        return $this->messagesModel->pullEmailMessages($messages);
    }

    public function pushMessages($limit)
    {
        return $this->messagesModel->pushMailMessages($limit);
    }

    public function removeMessageFromQueue($hashId)
    {
        return $this->messagesModel->removeMailFromQueue($hashId);
    }

    /**
     * @param $hashIds
     * @return bool|string
     * @throws MessagesServiceMailException
     */
    public function removeMessagesFromQueue($hashIds)
    {
        return $this->messagesModel->removeMailsFromQueue($hashIds);
    }

    public function getLogPattern()
    {
        return $this->config['log_file'];
    }

    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $emailParams JSON string with SMTP parameters
     * @return MailService|null
     */
    private function getMailService($emailParams)
    {
        $mailService = null;
        // Set custom email parameters
        $params = json_decode($emailParams);
        if ($params !== null) {
            /* @var $mailService MailService */
            $mailService = $this->serviceLocator->get('acmailer.mailservice.default');
            $mailService->getTransport()->getOptions()->setConnectionClass($params->authentication);
            $mailService->getTransport()->getOptions()->setHost($params->host);
            $mailService->getTransport()->getOptions()->setPort($params->port);
            $connectionParams = array(
                'username' => $params->username,
                'password' => $params->password,
            );
            if ($params->ssl !== null) {
                $connectionParams['ssl'] = $params->ssl;
            }
            $mailService->getTransport()->getOptions()->setConnectionConfig($connectionParams);

            if ($this->mailListener == null) {
                $this->mailListener = new MailListener();
                $this->mailListener->setLogger($this->messagesModel);
                $mailService->attachMailListener($this->mailListener);
            }
        }
        return $mailService;
    }

    public function updateMessage($hashId, Message $message)
    {
        return $this->messagesModel->updateMailMessage($hashId, $message);
    }

    /**
     * @param array $hashIds
     * @param string $message
     * @param string $sendDate
     * @param string $subject
     * @return bool|string
     * @throws MessagesServiceMailException
     */
    public function updateMessages(array $hashIds, $message = "", $sendDate = "", $subject = "")
    {
        return $this->messagesModel->updateMailMessages($hashIds, $message, $subject, $sendDate);
    }
}
