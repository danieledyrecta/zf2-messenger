# ZF2 Messenger Module

PHP library for easy message notifications.

## Requirements

* PHP 5.5+
* PHP Curl and OpenSSL modules
* Specific adapters requirements for push notifications (like APNS certificate, GCM API key, etc.)

### Installation

* * *

Install composer in your project

```bash
curl -s http://getcomposer.org/installer | php
```

In your `composer.json` add rhubbit git repository

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://dev_rhubbit@bitbucket.org/rhubbit/zf2-messenger.git"
    }
  ]
}
```

and add required module
```json
{
  "require": {
    "rhubbit/zf2-messenger": "1.1.*"
  }
}
```

Then run
```bash
php composer.phar install
```

Add the module `MessagesService` to your `config/application.config.php` file

```php
<?php

return [
    // This should be an array of module namespaces used in the application.
    'modules' => [
        'MessagesService',
        'Application'
    ],
];
```

Finally, run the following command to create the tables in the database:
```bash
php ./public/index.php zf2-messenger install
```

### Usage

* * *

After installation, you can add global parameters for all messages into a global.php file, for example `message.global.php`.

```php
<?php

return array(
    'acmailer_options' => [
        // Default mail service
        'default' => [
            'mail_adapter' => 'Zend\Mail\Transport\Smtp',
            'transport' => 'Zend\Mail\Transport\Smtp',

            'message_options' => [],

            'smtp_options' => [
                'host' => 'localhost',
                'port' => 25,
                'connection_class' => 'login', // The value can be one of 'smtp', 'plain', 'login' or 'crammd5'
                'connection_config' => [
                    'username' => 'user',
                    'password' => 'password',
                    'ssl' => null, // The value can be one of 'ssl', 'tls' or null
                ],
            ],

            'file_options' => [],
        ],
    ],
    'sms_message' => array(
        'sender.sms' => 'Info',
        'limit.max.tries' => 5,
        'limit.push.default' => 10,
        'log_file' => '/path/to/sms.log',
        'smsapi.user' => 'user',
        'smsapi.password' => 'password',
    ),
    'email_message' => array(
        'sender.email' => 'test@prova.it',
        'sender.alias' => 'Email Alias',
        'sender.label' => 'Test',
        'limit.max.tries' => 5,
        'limit.push.default' => 10,
        'log_file' => '/path/to/mail.log',
        'defaultTemplate' => 'email/only-text',
    ),
    'push_message' => array(
        'limit.max.tries' => 5,
        'limit.push.default' => 10,
        'log_file' => '/path/to/push.log',
    ),
);
```

You can use different parameters for each message adding them inside a JSON.
For example:
* email:
```json
{"username": "your_username","password": "your_password","authentication": "authentication_type","host": "your_smtp_host","port": 25,"ssl": null}
```
* sms:
```json
{"username": "your_username", "password": "your_password"}
```

* push notifications:
```json
{"badge": 2, "sound": "example.aiff"}
```

### Examples

Send a single message from terminal without going through queue:
```bash
php ./public/index.php message send push to 'cecMe3arGeI:APA91bFDZD_htMtLFBWul975V3cH1Z2CFTbhXeIb43XOuun5wSMcTZKX9jOnzjVIJAI1OGvWrkIZdbWR6dcLHDF8uVtoBLm8wtJP4oXXO6x8-gdT4rMPYjgRyWbMiS1oIJZ07VKob-rn' from 'Hub.plus' params '{"apiKey":"AAAAba4LSb0:APA91bF2G9LFUx4AkRD3xwYtwvDSGpEA5Rk-WCmpvVrTeLtEkuqTqcosfIlvShR9-ri5pFmL42jnFh-R8khSHWHO6uPUE71a5UaslzOl_6M1GtNYG9Bxs1DMf8mAUK-9Lpg3wRQ3pDvI","badge":1}' 'Messaggio PUSH di prova (nuovo giro params) :)' 'android'
php ./public/index.php message send email to 'info@test.com' from 'test@test.com' params '{"username": "user","password": "password","authentication":"login","host": "test.smtp.it","port": 25,"ssl": null}' 'Messaggio email di prova inviato dal modulo zf2-messenger' 'oggetto della mail'
php ./public/index.php message send sms to '+393921234567' from 'Info' params '{"username":"username","password":"password"}' 'Messaggio SMS di prova inviato dal modulo zf2-messenger'
```

Send messages from queue:
```bash
php ./public/index.php message send push
php ./public/index.php message send email
php ./public/index.php message send sms
```

Usage in ZF2 projects:

* Queue example

```php
<?php

use MessagesService\Model\MessagesModel,
    MessagesService\Model\MessageModel as Message,
    MessagesService\Service\Factory as MessagesFactory;

$message = new Message("Testo del messaggio da inviare...");

switch ($mode) {
    case MessagesFactory::MODE_SMS:
        $message->setOption('jsonParams', '{"username": "your_username", "password": "your_password"}');
        $message->setOption('from', $data['from']);
        $message->setOption('to', $data['to']);
        break;
    case MessagesFactory::MODE_MAIL:
        $message->setOption('jsonParams', "params from application config");
        $message->setOption('from', $data['from']);
        $message->setOption('senderLabel', $this->mail_sender_label);
        $message->setOption('to', $data['to']);
        $message->setOption('subject', $data['subject']);
        $message->setOption('templateLayout', $data['template']);
        break;
}

$msgModel = new MessagesModel($this->serviceLocator, $mode);
$msgModel->pullMessage($message);
```

* Sending the message immediately

```php
<?php

use MessagesService\Model\MessagesModel;

// Send message immediately

$params = '{"username": "your_username","password": "your_password","authentication": "authentication_type","host": "your_smtp_host","port": 25,"ssl": null}';

$msgModel = new MessagesModel($this->serviceLocator, $mode);
$msgModel->sendMessage(false, false, $params, "from@email.it", "to@email.it", "Your message content", "");
```